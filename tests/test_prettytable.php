<?php

require '../source/prettytable.php';

class ValidationError extends \Exception {}

class TestPrettyTable extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->table = new PrettyTable("Име", "Презиме", "Фамилия");
        $this->table->add_rows("RADOSLAV", "DIMITROV", "YORDANOV");
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public function test__constructor()
    {
        $table = new PrettyTable("Име", "Презиме", "Фамилия");
        $this->assertEquals(count($table->field_names), 3);
    }

    public function test_set_field_names()
    {
        $this->table->set_field_names(array("Име", "Презиме", "Фамилия"));
        $this->assertEquals(count($this->table->field_names), 3);
    }
    
    public function test___generate_table_returnStringRepresentationOfTable()
    {
        $table = $this->invokeMethod($this->table, 'generate_table', array());
        $this->assertTrue(strlen($table) > 5);
    }

    public function test___is_valid_InvalidRowCount()
    {   
        $table = new PrettyTable("Име", "Презиме", "Фамилия");
        $table->add_row("Radoslav", "Yordanov");
        try{
            $table = $this->invokeMethod($table, 'is_valid', array());
        } catch (ValidationError $e){
            $class = get_class($e);
        }
        $this->assertEquals($class, 'ValidationError');
    }       

    public function test___is_valid_RowCountEqualsToFieldNameCount()
    {
        $table = new PrettyTable("Име", "Презиме", "Фамилия");
        $table->add_rows("Radoslav", "DIMITROV", "Yordanov");
        $table = $this->invokeMethod($table, 'is_valid', array());
        $this->assertTrue($table);
    }

    public function test__toString_testEchoTrue()
    {
        $this->assertTrue(gettype((string)$this->table) == 'string');
    }

    public function test_toString_notValidRowCountThrowValidationError()
    {
        $this->table->add_rows("MARTIN", "KAMENOV");
        $this->assertEquals((string) $this->table, "Row count doesnt match Field name`s count");
    }
    
    public function test_clear_rows()
    {
        $this->table->clear_rows();
        $this->assertEquals(count($this->table->rows), 0);    
    }

    public function test___generate_rows_TestWithNoRows()
    {
        $this->table->clear_rows();
        try{
            $table = $this->invokeMethod($this->table, 'generate_rows', array('rows'=>array()));
        } catch (ValidationError $e) {
            $this->assertContains('Row count', $e->getMessage());
        }
    }

    public function test__generate_rows_TestWithCorrectData()
    {
        $this->assertContains('<table', (string) $this->table);
    }

    public function test___get_withParameter()
    {
        $table = new PrettyTable();
        $table->width='500px';
        $this->assertEquals($table->width, '500px');
    }

    public function test__set_withoutParameter()
    {
        $table = new PrettyTable();
    }

    public function test_add_sheet_withCorrectParametersReturnTrue()
    {
        $this->assertTrue($this->table->add_sheet("Direct"));
    }
    
    public function test_add_sheet_without_param()
    {
        $this->assertFalse($this->table->add_sheet());
    }
    
    public function test_insert_in_sheet_withCorrectParameters()
    {
        $name = "Direct";
        $rows = array("Radoslav", "Dimitrov", "Yordanov");
        $this->assertTrue($this->table->insert_in_sheet($name, $rows));
    }
    
    public function test_insert_in_sheet_withoutParameter()
    {
        $name = "Direct";
        $this->assertFalse($this->table->insert_in_sheet($name));
        $rows = array("Radoslav", "Dimitrov", "Yordanov");
        $this->assertFalse($this->table->insert_in_sheet($rows));
    }

    public function test_print_sheet()
    {
        $name = "direct";
        $table = new PrettyTable("Име","Презиме", "Фамилия");
        $table->add_sheet($name);
        $this->assertContains('<table', $table->print_sheet($name));
    }

    public function test_print_sheet_NameIsNullReturnFalse()
    {
        try{
            $this->table->print_sheet('sheet_that_doesnt_exist');
        } catch (ValidationError $e) {
            $class = get_class($e);
        }
        $this->assertEquals($class, 'ValidationError');
    }

    public function test_print_sheet_NameIsSetButDoesntExistThatSheet()
    {
        try{
            $this->table->print_sheet('sheet_that_doesnt_exist');
        } catch (ValidationError $e) {
            $class = get_class($e);
        }
        $this->assertEquals($class, 'ValidationError');
    }

}
