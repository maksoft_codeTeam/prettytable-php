<?php

require 'source/prettytable.php';


$simple_array = array(1 => 'January',
                      2 =>'February',
                      3 => 'March');                 

$table = new PrettyTable();
$table->add_sheet('direct');

$table->set_field_names(['#', 'Months']);
foreach ($simple_array as $number => $month):
        $table->add_row([$number, $month]);
        $table->insert_in_sheet('direct', [$number, $month]);
endforeach;



echo $table;

echo $table->print_sheet('direct');
