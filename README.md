# Print HTML Tables Easily #

This package was inspired by Python`s prettytable package. Fork it from github https://github.com/vishvananda/prettytable

### What is this repository for? ###

* Quick generate HTML tables

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Refactoring code
* Extend functionality

### How it works ###

One example is better than 1000 words


```
#!php

$simple_array = array(1 => 'January',
                      2 =>'February',
                      3 => 'March');    		     
    		      
$table = new PrettyTable();

$table->set_field_names(['#', 'Months']);
foreach ($simple_array as $number => $month):
	$table->add_row([$number, $month]);
endforeach;

echo $table;
```
